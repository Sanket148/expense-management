package org.dnyanyog.controller;

import javax.xml.xpath.XPathExpressionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.Test;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest

@AutoConfigureMockMvc

public class DirectoryControllerTest extends AbstractTestNGSpringContextTests {
	@Autowired
	MockMvc mocMvc;

	//New User SignUp
	@Test(priority = 1)
	public void verifyUsersSignupSuccessOpertionExceptionObject() throws XPathExpressionException, Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/directory/api/v1/signup").content

				("<User>\n"
				+ "<fullName>ram shinde</fullName>\n"
				+ "<email>ram@gmail.com</email>\n"
				+ "<password>ram@123</password>\n" 
				+ "<mobile>7475007800</mobile>\n"
				+ "<currency>USD</currency>\n" 
				+ "<language>English</language>\n" 
				+ "<country>US</country>\n"
				+ "</User>\n")
				.contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
		
		MvcResult result = mocMvc.perform(requestBuilder).andExpect(status().isCreated())
				.andExpect(xpath("/SignUpResponse/status").string("success"))
				.andExpect(xpath("/SignUpResponse/message").string("user account created successfully"))
				.andExpect(xpath("/SignUpResponse/data/fullName").string("ram shinde"))
			.andExpect(xpath("/SignUpResponse/data/email").string("ram@gmail.com"))
			.andExpect(xpath("/SignUpResponse/data/country").string("US"))
				.andExpect(xpath("/SignUpResponse/data/language").string("English"))
				.andExpect(xpath("/SignUpResponse/data/currency").string("USD"))
				.andExpect(xpath("/SignUpResponse/data/userId").string("1"))
				.andReturn();
	}


	@Test(priority = 2)
	public void verifyUsersSignupFailOpertionExistimgData() throws XPathExpressionException, Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/directory/api/v1/signup")
				.content("<User>\n"
						+ "<fullName>sumit suryawanshi</fullName>\n"
						+ "<email>ram@gmail.com</email>\n"
						+ "<password>ram@123</password>\n" 
						+ "<mobile>7475007800</mobile>\n"
						+ "<currency>INR</currency>\n" 
						+ "<language>Hindi</language>\n" 
						+ "<country>IND</country>\n"
						+ "</User>\n")
				.contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
		
		MvcResult result = mocMvc.perform(requestBuilder).andExpect(status().isConflict())
				.andExpect(xpath("/SignUpResponse/status").string("error"))
				.andExpect(xpath("/SignUpResponse/message").string("Email or mobile number already registered"))

				.andReturn();
	}

	
	@Test(priority = 2)
	public void verifyUsersSignupFailOpertionMobileExist() throws XPathExpressionException, Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/directory/api/v1/signup")
				.content("<User>\n"
						+ "<fullName>sumit suryawanshi</fullName>\n"
						+ "<email>ram1@gmail.com</email>\n"
						+ "<password>ram@123</password>\n" 
						+ "<mobile>7475007800</mobile>\n"
						+ "<currency>USD</currency>\n" 
						+ "<language>English</language>\n" 
						+ "<country>US</country>\n"
						+ "</User>\n")
				.contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
		
		MvcResult result = mocMvc.perform(requestBuilder).andExpect(status().isConflict())
				.andExpect(xpath("/SignUpResponse/status").string("error"))
				.andExpect(xpath("/SignUpResponse/message").string("Email or mobile number already registered"))

				.andReturn();
	}

	
@Test(priority = 2)
	public void verifyUsersSignupFailOpertionEmailExist() throws XPathExpressionException, Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/directory/api/v1/signup")
				.content("<User>\n"
						+ "<fullName>sumit suryawanshi</fullName>\n"
						+ "<email>ram@gmail.com</email>\n"
						+ "<password>ram@123</password>\n" 
						+ "<mobile>7475007500</mobile>\n"
						+ "<currency>USD</currency>\n" 
						+ "<language>English</language>\n" 
						+ "<country>US</country>\n"
						+ "</User>\n")
				.contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
		
		MvcResult result = mocMvc.perform(requestBuilder).andExpect(status().isConflict())
				.andExpect(xpath("/SignUpResponse/status").string("error"))
				.andExpect(xpath("/SignUpResponse/message").string("Email or mobile number already registered"))

				.andReturn();
	}
	
	

	
	
	
	
	@Test
	public void verifyUsersSignUpnewfriend()throws XPathExpressionException,Exception{
		RequestBuilder requestBuilder =MockMvcRequestBuilders.post("/friends/api/v1/create").content("<user>\r\n"
				+ "  <fullName>sagar gonjare</fullName>\r\n"
				+ "  <email>sagar111@example.com</email>\r\n"
				+ "  <mobileNo>8888888898</mobileNo>\r\n"
				+ "</user>\r\n"
				+ "").contentType(MediaType.APPLICATION_XML_VALUE).accept(MediaType.APPLICATION_XML_VALUE);
		
		MvcResult result =mocMvc.perform(requestBuilder).andExpect(status().isCreated())
				.andExpect(xpath("/GenericResponse/status").string("success"))
				.andExpect(xpath("/GenericResponse/message").string("User account created successfully"))
				.andReturn();
}
}