package org.dnyanyog.repository;

import org.dnyanyog.entity.GroupsInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository  extends JpaRepository<GroupsInformation,Long>{

	GroupsInformation findByGroupName(String groupName);

}
