package org.dnyanyog.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.dnyanyog.entity.Users;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class PrePostProcessHook {
	
	private static final Logger logger = LoggerFactory.getLogger(PrePostProcessHook.class);

	/*@Before("execution(* org.dnyanyog.repository.*.save(..))")
	public void beforeExecution(JoinPoint jointPoint) {
		System.out.println("\n****************Saving object -" + jointPoint.getArgs()[0]);

		Users users = (Users) jointPoint.getArgs()[0];
		logger.info(users.getFullName());
		logger.info(users.getMobileNo());
		logger.info(users.getEmail());
		logger.info(users.getPassword());
		logger.info(users.getCountry());
		logger.info(users.getLanguage());
		logger.info(users.getCurrency());

	}

	@After("execution(* org.dnyanyog.repository.*.save(..))")
	public void afterExecution(JoinPoint jointPoint) {
		System.out.println("\n****************Saved object -" + jointPoint.getArgs()[0]);
		Users users = (Users) jointPoint.getArgs()[0];

		logger.info(users.getFullName());
		logger.info(users.getMobileNo());
		logger.info(users.getEmail());
		logger.info(users.getPassword());
		logger.info(users.getCountry());
		logger.info(users.getLanguage());
		logger.info(users.getCurrency());
	}*/

}
